import {Link} from 'react-router-dom';

function Main() {
    return ( 
        <div>
            <h1>Pagina principal</h1>
            <Link to={'/listar_funcoes'}>Listar Funções</Link>
        </div>
    );
}

export default Main;