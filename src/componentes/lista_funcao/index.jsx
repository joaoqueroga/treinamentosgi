import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import api from "../../config/api";
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { FilterMatchMode } from 'primereact/api';

function ListaFuncao(){
    const token = '5d114bb0a447402994bd5df7d237cd2a69f68bf8';

    const [nome] = useState('Joao Queroga');
    const [idade, setIdade] = useState(0);

    const [funcoes, setFuncoes] = useState([]);

    const [filters] = useState({
        'id_funcao': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'descricao': { value: null, matchMode: FilterMatchMode.CONTAINS }
    });

    useEffect(()=>{
        let config  = { headers : {"Authorization" : `Token ${token}`} }
        api.get('/rh/funcoes/', config).then((resposta)=>{
            let x = JSON.parse(resposta.data)
            console.log(x);
            setFuncoes(x.funcao);
        }).catch((erro)=>{
            console.log(erro);
        })
    },[])

    return(
        <div>
            <Link to={'/'}>Inicio</Link>
            <h2> Nome: { nome } </h2>
            <h2> Idade: {idade} </h2>
            <button onClick={()=>{ setIdade( idade - 1) }}> - </button>
            <button onClick={()=>{ setIdade( idade + 1) }}> + </button>
            <DataTable value={funcoes} responsiveLayout="scroll"  filters={filters} filterDisplay="row">
                    <Column field="id_funcao" header="Id" filter  ></Column>
                    <Column field="descricao" header="Descrição" filter></Column>
            </DataTable>
        </div>
    )
}

export default ListaFuncao;