import {BrowserRouter, Route, Routes} from 'react-router-dom';

import ListaFuncao from './componentes/lista_funcao';
import Main from './componentes/main';

function Rotas(){
    return(
        <BrowserRouter>
            <Routes>
                <Route 
                    path='/listar_funcoes'
                    element={<ListaFuncao/>}
                />
                <Route 
                    path='/'
                    element={<Main/>}
                />


                <Route path='*' element={<div>404</div>}/>
            </Routes>
        </BrowserRouter>
    )
}

export default Rotas;